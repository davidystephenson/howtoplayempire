var express = require('express');
var http = require('http');
var socketIo = require('socket.io');
var path = require('path');

var app = express();
var server = http.Server(app);
var io = socketIo(server);

app.set('port', process.env.PORT || 3000);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(request, response) {
  response.render('index', { title: 'How to Play Empire' });
});

io.on('connection', function(socket){
  socket.on('hello', function() { console.log('hello'); });
});

server.listen(app.get('port'), function () { console.log('listening on', app.get('port')); });
